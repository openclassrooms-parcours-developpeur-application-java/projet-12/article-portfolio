package com.articleportfolio.controller;

import com.articleportfolio.exception.ProjectNotFoundException;
import com.articleportfolio.proxy.MicroserviceMediaProxy;
import com.articleportfolio.repository.ArticleRepository;
import com.articleportfolio.repository.ParagraphRepository;
import com.articleportfolio.repository.ResourceRepository;
import com.articleportfolio.repository.TagRepository;
import com.articleportfolio.service.dto.*;
import com.articleportfolio.service.mapper.ArticleMapper;
import com.articleportfolio.service.mapper.ParagraphMapper;
import com.articleportfolio.service.mapper.ResourceMapper;
import com.articleportfolio.service.mapper.TagMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Api(description = "API POUR LES OPERATIONS CRUD SUR LES ARTICLE FOR PROJECT" )
@RestController
public class ProjectArticleController {

    private static final Logger logger = LoggerFactory.getLogger(ProjectArticleController.class);

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ParagraphRepository paragraphRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private MicroserviceMediaProxy microserviceMediaProxy;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* ------------------------------------- GET ALL ARTICLE BY PROJECT ------------------------------------- */
    @ApiOperation(value = "GET ALL ARTICLE BY PROJECT")
    @GetMapping(value = "/article/project/get-all-article/{projectId}")
    public List<ArticleDTO> getListArticleByProject(@PathVariable Integer projectId) {

        List<ArticleDTO> articleDTOList = new ArrayList<>();

        /**
         * OBTENIR LA LISTE DES ARTICLES POUR UN PROJET
         * @see ArticleRepository#findAllByProjectId(Integer)
         */
        articleDTOList = ArticleMapper.INSTANCE.toDTOList(articleRepository.findAllByProjectId(projectId));

        if (articleDTOList != null) {

            /**
             * TRIER LES ARTICLES PAR ORDRE CROISSANT EN FONCTION DE LEURS POSITIONS
             */
            Collections.sort(articleDTOList, new Comparator<ArticleDTO>() {
                @Override
                public int compare(ArticleDTO o1, ArticleDTO o2) {
                    return o1.getPosition().compareTo(o2.getPosition());
                }
            });

            for (ArticleDTO articleDTO : articleDTOList) {

                /**
                 * OBTENIR LA LISTE DES PARAGRAPHES POUR UN ARTICLE
                 * @see ParagraphRepository#findAllByArticleId(Integer)
                 */
                articleDTO.setParagraphDTOList(ParagraphMapper.INSTANCE.toDTOList(
                        paragraphRepository.findAllByArticleId(articleDTO.getId())));

                /**
                 * TRIER LES PARAGRAPHES PAR ORDRE CROISSANT EN FONCTION DE LEURS POSITIONS POUR UN ARTICLE
                 */
                Collections.sort(articleDTO.getParagraphDTOList(), new Comparator<ParagraphDTO>() {
                    @Override
                    public int compare(ParagraphDTO o1, ParagraphDTO o2) {
                        return o1.getPosition().compareTo(o2.getPosition());
                    }
                });

                /**
                 * OBTENIR LA LISTE DES IMAGES POUR CHAQUE PARAGRAPHE
                 */
                for (ParagraphDTO paragraphDTO : articleDTO.getParagraphDTOList()) {
                    /** @see MicroserviceMediaProxy#getListImageByParagraph(Integer) */
                    paragraphDTO.setImageDTOList(microserviceMediaProxy.getListImageByParagraph(paragraphDTO.getId()));
                }
            }

            /**
             * OBTENIR LA LISTE DES "RESOURCES" ET DES "TAGS" POUR CHAQUE ARTCLE
             */
            for (ArticleDTO articleDTO : articleDTOList) {

                /** @see ResourceRepository#findAllByArticleId(Integer) */
                articleDTO.setResourceDTOList(ResourceMapper.INSTANCE.toDTOList(
                        resourceRepository.findAllResourceByArticleId(articleDTO.getId())));

                /** @see TagRepository#findAllByArticleId(Integer) */
                articleDTO.setTagDTOList(TagMapper.INSTANCE.toDTOList(
                        tagRepository.findAllTagByArticleId(articleDTO.getId())));
            }

        } else {
            throw new ProjectNotFoundException("Project not found for the ID : " + projectId +
                    " - Cannot retrieve articles related to the project");
        }
        return articleDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ------------------------------------- ADD ARTICLE FOR PROJECT ------------------------------------- */
    @ApiOperation(value = "ADD ARTICLE FOR PROJET ")
    @PostMapping(value = "/article/project/add-article/{projectId}")
    public List<ArticleDTO> addArticleForProject(@RequestBody List<ArticleDTO> articleDTOList,
                                                 @PathVariable Integer projectId) {

        /**
         * JE VERIFIE SI LE PROJET EXISTE
         */
        Boolean projectExists = articleRepository.projectExists(projectId);

        if (projectExists) {

            for(ArticleDTO articleDTO : articleDTOList) {

                /** J'AJOUTE A L'ARTICLE L'ID DU PROJET AUQUEL CELUI-CI EST LIE */
                articleDTO.setProjectId(projectId);

                try {

                    /** J'AJOUTE L'ARTICLE, PUIS JE RECUPERE SON IDENTIFIANT */
                    articleDTO.setId(ArticleMapper.INSTANCE.toDTO
                            (articleRepository.save(ArticleMapper.INSTANCE.toEntity(articleDTO))).getId());
                } catch (Exception pEX) {
                    logger.error(String.valueOf(pEX));
                }
            }

        } else {
            throw new ProjectNotFoundException("Project not found for the ID : " + projectId +
                    " - Impossible to add article to the project");
        }
        return articleDTOList;
    }

}
