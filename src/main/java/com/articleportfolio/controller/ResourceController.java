package com.articleportfolio.controller;

import com.articleportfolio.exception.ResouceNotFoundException;
import com.articleportfolio.exception.ResourceConflictException;
import com.articleportfolio.repository.ResourceRepository;
import com.articleportfolio.service.dto.ResourceDTO;
import com.articleportfolio.service.mapper.ResourceMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "API POUE LES OPERATIONS CRUD SUR LES RESSOURCES")
@RestController
public class ResourceController {

    private static final Logger logger = LoggerFactory.getLogger(ArticleController.class);

    @Autowired
    private ResourceRepository resourceRepository;


                                    /* ===================================== */
                                    /* ============== UPDATE =============== */
                                    /* ===================================== */
    /* ------------------------------------------ UP RESSOURCE ------------------------------------------ */
    @ApiOperation(value = "UP RESSOURCE FOR ARTICLE")
    @PutMapping(value = "/article/resource/up-ressource")
    public ResourceDTO upRessource(@RequestBody ResourceDTO resourceDTO) {

        /**
         * JE VERIFIE SI LA RESSOURCE EXISTE
         */
        Boolean resourceExists = resourceRepository.existsById(resourceDTO.getId());

        if (resourceExists) {

            try {
                resourceDTO = ResourceMapper.INSTANCE.toDTO(
                        resourceRepository.save(ResourceMapper.INSTANCE.toEntity(resourceDTO)));
            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - resource : '" + resourceDTO.getName() + "' - Is not found");
            throw new ResouceNotFoundException("Update failure - resource : '" + resourceDTO.getName() + "' - Is not found");
        }
        return resourceDTO;
    }


                                    /* ======================================= */
                                    /* =============== DELETE  =============== */
                                    /* ======================================= */
    /* ----------------------------------------- DEL RESSOURCE ----------------------------------------- */
    @ApiOperation(value = "DEL RESSOURCE")
    @DeleteMapping(value = "/article/resource/del-ressource/{resourceId}")
    public String delRessource(@PathVariable Integer resourceId) {

        String massageStatus = "";

        /**
         * JE VERIFIE SI L'IMAGE EST LIEE AVEC UN PROJET, PROOF OF CONCEPT...
         */
        String resourceUseInTheTableProject = resourceRepository.resourceUseInTheTableProject(resourceId);
        String resourceUseInTheTableProfOfConcept = resourceRepository.resourceUseInTheTableProfOfConcept(resourceId);
        String resourceUseInTheTableArticle = resourceRepository.resourceUseInTheTableArticle(resourceId);

        /**
         * SI L'IMAGE EST LIEE, JE RENVOIE UN MESSAGE INDIQUANT LA PROCEDURE A SUIVRE POUR SA SUPPRESSION
         * EST A QUI L'IMAGE EST LIEE
         */
        if (resourceUseInTheTableProject != null || resourceUseInTheTableProfOfConcept != null
                || resourceUseInTheTableArticle != null) {

            if (resourceUseInTheTableProject != null)
                throw new ResourceConflictException("The resource is linked to the project '"
                        + resourceUseInTheTableProject + "' - To remove this resource you first had to remove the link that links " +
                        "them in the project section.");

            if (resourceUseInTheTableProfOfConcept != null)
                throw new ResourceConflictException("The resource is linked to the proof Of Concept '"
                        + resourceUseInTheTableProfOfConcept + "' - To remove this resource you first had to remove the link that " +
                        "links them in the proof Of Concept section.");

            if (resourceUseInTheTableArticle != null)
                throw new ResourceConflictException("The resource is linked of the article'"
                        + resourceUseInTheTableArticle + "' - To remove this resource you first had to remove the link that" +
                        " links them in the article section.");

        } else {

            try {

                Integer check = resourceRepository.deleteResource(resourceId);

                if (check == 0) {
                    massageStatus = "Failed deletion - image not found";
                } else {
                    massageStatus = "Successful deletion of the image";
                }

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }
        }
        return massageStatus;
    }
}
