package com.articleportfolio.controller;

import com.articleportfolio.exception.ResouceNotFoundException;
import com.articleportfolio.exception.TagConflictException;
import com.articleportfolio.exception.TagNotFoundException;
import com.articleportfolio.repository.TagRepository;
import com.articleportfolio.service.dto.TagDTO;
import com.articleportfolio.service.mapper.TagMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "API POUE LES OPERATIONS CRUD SUR LES TAGS")
@RestController
public class TagController {

    private static final Logger logger = LoggerFactory.getLogger(ArticleController.class);

    @Autowired
    private TagRepository tagRepository;


                                    /* ===================================== */
                                    /* ============== UPDATE =============== */
                                    /* ===================================== */
    /* -------------------------------------------- UP TAG -------------------------------------------- */
    @ApiOperation(value = "UP TAG")
    @PutMapping(value = "/article/tag/up-tag")
    public TagDTO upTag(@RequestBody TagDTO tagDTO) {

        /**
         * JE VERIFIE SI LE TAG EXISTE
         */
        Boolean tagExists = tagRepository.existsById(tagDTO.getId());

        if (tagExists) {

            try {
                tagDTO = TagMapper.INSTANCE.toDTO(tagRepository.save(TagMapper.INSTANCE.toEntity(tagDTO)));
            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }

        } else {
            logger.error("Update failure - tag : '" + tagDTO.getName() + "' - Is not found");
            throw new TagNotFoundException("Update failure - tag : '" + tagDTO.getName() + "' - Is not found");
        }
        return tagDTO;
    }

                                /* ======================================= */
                                /* =============== DELETE  =============== */
                                /* ======================================= */

    /* ------------------------------------- DEL TAG ------------------------------------- */
    @ApiOperation(value = "DEL TAG")
    @DeleteMapping(value = "/article/tag/del-tag/{tagId}")
    public String delTag(@PathVariable Integer tagId) {

        String massageStatus = "";

        /**
         * JE VERIFIE SI LE ATG EST LIEE AVEC UN ARTICLE OU UN POINT OF INTEREST
         */
        String tagUseInTheTableArticle = tagRepository.tagUseInTheTableArticle(tagId);
        String tagUseInTheTablePointOfInterest = tagRepository.tagUseInTheTablePointOfInterest(tagId);

        /**
         * SI LE TAG EST LIEE, JE RENVOIE UN MESSAGE INDIQUANT LA PROCEDURE A SUIVRE POUR SA SUPPRESSION
         * EST A QUI LE TAG EST LIEE
         */
        if (tagUseInTheTableArticle != null || tagUseInTheTablePointOfInterest != null) {

            if (tagUseInTheTableArticle != null)
                throw new TagConflictException("The tag is linked of the article '"
                        + tagUseInTheTableArticle + "' - To remove this tag you first had to remove the link that" +
                        " links them in the article section.");

            if (tagUseInTheTablePointOfInterest != null)
                throw new TagConflictException("The tag is linked to the point of interest '"
                        + tagUseInTheTablePointOfInterest + "' - To remove this tag you first had to remove the link that links " +
                        "them in the point of interest section.");

        } else {

            try {

                Integer check = tagRepository.deleteTag(tagId);

                if (check == 0) {
                    massageStatus = "Failed deletion - image not found";
                } else {
                    massageStatus = "Successful deletion of the image";
                }

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }
        }
        return massageStatus;
    }
}

