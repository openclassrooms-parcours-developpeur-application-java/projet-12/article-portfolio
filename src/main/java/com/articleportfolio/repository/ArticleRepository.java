package com.articleportfolio.repository;

import com.articleportfolio.model.ArticleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<ArticleEntity, Integer> {


    /* ------------------------------------- PROJECT ------------------------------------- */
    /**
     * OBTENIR TOUTES LES ARTICLES PAR L'ID DU PROJET
     * @param projectId
     * @return
     */
    List<ArticleEntity> findAllByProjectId(Integer projectId);


    /**
     * VERIFIE SI LE PROJET EXISTE
     * @param projectId
     * @return
     */
    @Query(value = "SELECT COUNT(project) > 0 FROM project WHERE project.id= :projectId", nativeQuery = true)
    Boolean projectExists(@Param("projectId") Integer projectId);

    /* ------------------------------------- PROOF OF CONCEPT ------------------------------------- */
    /**
     * OBTENIR TOUTES LES ARTICLES PAR L'ID DU PROOF OF CONCEPT
     * @param poId
     * @return
     */
    List<ArticleEntity> findAllByProofOfConceptId(Integer poId);

    /**
     * VERIFIE SI LE PROOF OF CONCEPT EXISTE
     * @param poId
     * @return
     */
    @Query(value = "SELECT COUNT(proof_of_concept) > 0 FROM proof_of_concept WHERE proof_of_concept.id= :poId", nativeQuery = true)
    Boolean proofOfConceptExists(@Param("poId") Integer poId);


    /* ------------------------------------- ARTICLE ------------------------------------- */
    /**
     * SUPPRIMER UN ARTICLE PAR SON ID
     * @param articleId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM article" +
            " WHERE article.id= :articleId", nativeQuery = true)
    int deleteArtcileById(@Param("articleId") Integer articleId);

}
