package com.articleportfolio.repository;

import com.articleportfolio.model.ResourceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ResourceRepository extends JpaRepository<ResourceEntity, Integer> {


    /**
     * OBTENIR TOUTES LES RESOURCES PAR L'ID DE L'ARTICLE
     * @param articleId
     * @return
     */
    @Query(value = "SELECT DISTINCT resource.* FROM  resource" +
            " INNER JOIN resource_article ON resource_article.article_id= :articleId" +
            " WHERE resource.id= resource_article.resource_id", nativeQuery = true)
    List<ResourceEntity> findAllResourceByArticleId(@Param("articleId") Integer articleId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE RESOURCE ET ARTICLE EXISTE DEJA
     * @param resource_id
     * @param articleId
     * @return
     */
    @Query(value = "SELECT count(resource_article) > 0 FROM resource_article" +
            " WHERE resource_article.resource_id= :resource_id AND resource_article.article_id= :articleId", nativeQuery = true)
    Boolean resourceArticleExists(@Param("resource_id") Integer resource_id, @Param("articleId") Integer articleId);


    /**
     * LIER DANS LA TABLE DE JOINTURE LA RESOURCE A L'ARTICLE
     * @param resourceId
     * @param articleId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO resource_article (resource_id, article_id)" +
            " VALUES (:resource_id, :article_id)", nativeQuery = true)
    int joinResourceToTheArticle(@Param("resource_id") Integer resourceId, @Param("article_id") Integer articleId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UNE RESOURCE ET UN ARTICLE
     * @param ressourceId
     * @param articleId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM resource_article" +
            " WHERE resource_id= :ressourceId AND article_id= :articleId", nativeQuery = true)
    int deleteResourceForArticle(@Param("ressourceId") Integer ressourceId, @Param("articleId") Integer articleId);


    /**
     * VERIFIE SI LE ARTICLE EXISTE
     * @param articleId
     * @return
     */
    @Query(value = "SELECT COUNT(article) > 0 FROM article WHERE article.id= :articleId", nativeQuery = true)
    Boolean articleExists(@Param("articleId") Integer articleId);


    /**
     * OBTENIR LA RESOURCE PAR SON URL
     * @param url
     * @return
     */
    ResourceEntity findByUrl(String url);


    /**
     * VERIFIE SI LA RESSOURCE ET UTILISER DANS LA TABLE PROJECT
     * @param ressourceId
     * @return
     */
    @Query(value = "SELECT DISTINCT name FROM project " +
            " INNER JOIN resource_project ON resource_project.resource_id= :ressourceId" +
            " WHERE project.id= resource_project.project_id", nativeQuery = true)
    String resourceUseInTheTableProject(@Param("ressourceId") Integer ressourceId);


    /**
     * VERIFIE SI LA RESSOURCE ET UTILISER DANS LA TABLE PROOF OF CONCEPT
     * @param ressourceId
     * @return
     */
    @Query(value = "SELECT DISTINCT name FROM proof_of_concept" +
            " INNER JOIN resource_proofofconcept ON resource_proofofconcept.resource_id= :ressourceId" +
            " WHERE proof_of_concept.id= resource_proofofconcept.proof_of_concept_id", nativeQuery = true)
    String resourceUseInTheTableProfOfConcept(@Param("ressourceId") Integer ressourceId);


    /**
     * VERIFIE SI LA RESSOURCE ET UTILISER DANS LA TABLE ARTICLE
     * @param ressourceId
     * @return
     */
    @Query(value = "SELECT DISTINCT title FROM article" +
            " INNER JOIN resource_article ON resource_article.resource_id= :ressourceId" +
            " WHERE article.id= resource_article.article_id", nativeQuery = true)
    String resourceUseInTheTableArticle(@Param("ressourceId") Integer ressourceId);


    /**
     * SUPPRIMER LA RESSOURCE
     * @param ressourceId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM resource" +
            " WHERE resource.id= :ressourceId", nativeQuery = true)
    int deleteResource(@Param("ressourceId") Integer ressourceId);
}
