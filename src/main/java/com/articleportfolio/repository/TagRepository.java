package com.articleportfolio.repository;

import com.articleportfolio.model.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<TagEntity, Integer> {


    /**
     * OBTENIR TOUTES LES TAGS PAR L'ID DE L'ARTICLE
     * @param articleId
     * @return
     */
    @Query(value = "SELECT DISTINCT tag.* FROM tag" +
            " INNER JOIN tag_article ON tag_article.article_id= :articleId" +
            " WHERE tag.id= tag_article.tag_id", nativeQuery = true)
    List<TagEntity> findAllTagByArticleId(@Param("articleId") Integer articleId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE TAG ET ARTICLE EXISTE DEJA
     * @param tag_id
     * @param articleId
     * @return
     */
    @Query(value = "SELECT count(tag_article) > 0 FROM tag_article" +
            " WHERE tag_article.tag_id= :tag_id AND tag_article.article_id= :articleId", nativeQuery = true)
    Boolean tagArticleExists(@Param("tag_id") Integer tag_id, @Param("articleId") Integer articleId);


    /**
     * LIER DANS LA TABLE DE JOINTURE LE TAG A L'ARTICLE
     * @param tagId
     * @param articleId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO tag_article (tag_id, article_id)" +
            " VALUES (:tag_id, :article_id)", nativeQuery = true)
    int joinTagToTheArticle(@Param("tag_id") Integer tagId, @Param("article_id") Integer articleId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UN TAG ET UN ARTICLE
     * @param tagId
     * @param articleId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM tag_article" +
            " WHERE tag_id= :tagId AND  article_id= :articleId", nativeQuery = true)
    int deleteTagForArticle(@Param("tagId") Integer tagId, @Param("articleId") Integer articleId);


    /**
     * VERIFIE SI LE ARTICLE EXISTE
     * @param articleId
     * @return
     */
    @Query(value = "SELECT COUNT(article) > 0 FROM article WHERE article.id= :articleId", nativeQuery = true)
    Boolean articleExists(@Param("articleId") Integer articleId);


    /**
     * OBTENIR LA RESOURCE PAR SON PATH
     * @param tagPath
     * @return
     */
    TagEntity findByTagPath(String tagPath);


    /**
     * VERIFIE SI LE TAG ET UTILISER DANS LA TABLE ARTICLE
     * @param tagId
     * @return
     */
    @Query(value = "SELECT DISTINCT title FROM article" +
            " INNER JOIN tag_article ON tag_article.tag_id= :tagId" +
            " WHERE article.id= tag_article.article_id", nativeQuery = true)
    String tagUseInTheTableArticle(@Param("tagId") Integer tagId);


    /**
     * VERIFIE SI LE TAG ET UTILISER DANS LA TABLE POINT OF INTEREST
     * @param tagId
     * @return
     */
    @Query(value = "SELECT DISTINCT name FROM point_of_interest" +
            " INNER JOIN tag_pointofinterest ON tag_pointofinterest.tag_id= :tagId" +
            " WHERE point_of_interest.id= tag_pointofinterest.point_of_interest_id", nativeQuery = true)
    String tagUseInTheTablePointOfInterest(@Param("tagId") Integer tagId);


    /**
     * SUPPRIMER LE TAG
     * @param tagId
     * @return
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM tag" +
            " WHERE tag.id= :tagId", nativeQuery = true)
    int deleteTag(@Param("tagId") Integer tagId);
}
