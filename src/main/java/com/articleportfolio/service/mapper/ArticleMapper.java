package com.articleportfolio.service.mapper;

import com.articleportfolio.model.ArticleEntity;
import com.articleportfolio.service.dto.ArticleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper
public interface ArticleMapper {

    ArticleMapper INSTANCE = Mappers.getMapper( ArticleMapper.class );

    ArticleDTO toDTO ( ArticleEntity articleEntity );

    List<ArticleDTO> toDTOList ( List<ArticleEntity> articleEntityList );

    ArticleEntity toEntity ( ArticleDTO articleDTO );

    List<ArticleEntity> toEntityList ( List<ArticleDTO> articleDTOList );
}
