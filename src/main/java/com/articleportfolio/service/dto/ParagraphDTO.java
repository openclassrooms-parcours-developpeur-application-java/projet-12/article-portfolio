package com.articleportfolio.service.dto;

import lombok.*;

import java.util.List;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class ParagraphDTO {

    private Integer id;

    private String subtitle;

    private String paragraph;

    private Integer position;

    private Integer articleId;

    private List<ImageDTO> imageDTOList;

}
