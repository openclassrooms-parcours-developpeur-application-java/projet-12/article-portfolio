package com.articleportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ParagraphNotFoundException extends RuntimeException {

    public ParagraphNotFoundException(String s) {
        super(s);
    }
}
