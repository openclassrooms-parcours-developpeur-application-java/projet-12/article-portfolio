package com.articleportfolio.controller;

import com.articleportfolio.exception.ArticleConflictException;
import com.articleportfolio.exception.ArticleNotFoundException;
import com.articleportfolio.exception.ParagraphNotFoundException;
import com.articleportfolio.model.ParagraphEntity;
import com.articleportfolio.model.ResourceEntity;
import com.articleportfolio.model.TagEntity;
import com.articleportfolio.repository.ArticleRepository;
import com.articleportfolio.repository.ParagraphRepository;
import com.articleportfolio.repository.ResourceRepository;
import com.articleportfolio.repository.TagRepository;
import com.articleportfolio.service.dto.ArticleDTO;
import com.articleportfolio.service.dto.ParagraphDTO;
import com.articleportfolio.service.dto.ResourceDTO;
import com.articleportfolio.service.dto.TagDTO;
import com.articleportfolio.service.mapper.ArticleMapper;
import com.articleportfolio.service.mapper.ParagraphMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ArticleControllerTest {

    @Mock
    ArticleRepository articleRepository;

    @Mock
    ParagraphRepository paragraphRepository;

    @Mock
    ResourceRepository resourceRepository;

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    ArticleController articleController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addParagraphForArticle() {

        List<ParagraphDTO> paragraphDTOListMock = new ArrayList<>();
        Integer articleIdMock  = 5;

        ParagraphEntity paragraphEntityMock = ParagraphEntity.builder().id(1).position(1).subtitle("sout-titre 1")
                .paragraph("Paragraphe 1").articleId(1).build();
        ParagraphDTO paragraphDTOMock1 = ParagraphDTO.builder().position(1).subtitle("sout-titre 1")
                .paragraph("Paragraphe 1").articleId(1).build();
        paragraphDTOListMock.add(paragraphDTOMock1);

        when(articleRepository.existsById(articleIdMock)).thenReturn(true);
        when(paragraphRepository.save(any())).thenReturn(paragraphEntityMock);

        final List<ParagraphDTO> paragraphDTOList = articleController.addParagraphForArticle(paragraphDTOListMock, articleIdMock);
        Assertions.assertEquals(paragraphDTOList.get(0).getId(), paragraphDTOMock1.getId());

        when(articleRepository.existsById(articleIdMock)).thenReturn(false);
        Assertions.assertThrows(ParagraphNotFoundException.class, () -> {
            articleController.addParagraphForArticle(paragraphDTOListMock, articleIdMock);
        });
    }

    @Test
    void addRessourceForArticle() {

        List<ResourceDTO> resourceDTOListMock = new ArrayList<>();
        Integer articleIdMock  = 5;

        ResourceEntity resourceEntityMock = ResourceEntity.builder().id(1).name("Nom de la ressource")
                .url("URL de la ressource").build();
        ResourceDTO resourceDTOMock = ResourceDTO.builder().name("Nom de la ressource")
                .url("URL de la ressource").build();
        resourceDTOListMock.add(resourceDTOMock);

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceArticleExists(resourceEntityMock.getId(), articleIdMock)).thenReturn(false);
        when(resourceRepository.joinResourceToTheArticle(resourceEntityMock.getId(), articleIdMock)).thenReturn(1);

        final List<ResourceDTO> resourceDTOList1 = articleController.addRessourceForArticle(resourceDTOListMock, articleIdMock);
        for (ResourceDTO resourceDTO : resourceDTOList1) {
            Assertions.assertEquals(resourceDTO.getId(), 1);
        }

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(null);
        when(resourceRepository.articleExists(articleIdMock)).thenReturn(true);
        when(resourceRepository.save(any())).thenReturn(resourceEntityMock);
        when(resourceRepository.joinResourceToTheArticle(resourceEntityMock.getId(), articleIdMock)).thenReturn(1);

        final List<ResourceDTO> resourceDTOList2 = articleController.addRessourceForArticle(resourceDTOListMock, articleIdMock);
        for (ResourceDTO resourceDTO : resourceDTOList2) {
            Assertions.assertEquals(resourceDTO.getId(), 1);
        }

    }

    @Test
    void addRessourceForArticleException() {

        List<ResourceDTO> resourceDTOListMock = new ArrayList<>();
        Integer articleIdMock  = 5;

        ResourceEntity resourceEntityMock = ResourceEntity.builder().id(1).name("Nom de la ressource")
                .url("URL de la ressource").build();
        ResourceDTO resourceDTOMock = ResourceDTO.builder().name("Nom de la ressource")
                .url("URL de la ressource").build();
        resourceDTOListMock.add(resourceDTOMock);

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceArticleExists(resourceEntityMock.getId(), articleIdMock)).thenReturn(true);
        Assertions.assertThrows(ArticleConflictException.class, () -> {
            articleController.addRessourceForArticle(resourceDTOListMock, articleIdMock);
        });

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(resourceEntityMock);
        when(resourceRepository.resourceArticleExists(resourceEntityMock.getId(), articleIdMock)).thenReturn(false);
        when(resourceRepository.joinResourceToTheArticle(resourceEntityMock.getId(), articleIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ArticleNotFoundException.class, () -> {
            articleController.addRessourceForArticle(resourceDTOListMock, articleIdMock);
        });

        when(resourceRepository.findByUrl(resourceDTOMock.getUrl())).thenReturn(null);
        when(resourceRepository.resourceArticleExists(resourceEntityMock.getId(), articleIdMock)).thenReturn(true);
        when(resourceRepository.articleExists(articleIdMock)).thenReturn(false);
        Assertions.assertThrows(ArticleNotFoundException.class, () -> {
            articleController.addRessourceForArticle(resourceDTOListMock, articleIdMock);
        });
    }

    @Test
    void addTagForArticle() {

        List<TagDTO> tagDTOListMock = new ArrayList<>();
        Integer articleIdMock  = 5;

        TagEntity tagEntityMock = TagEntity.builder().id(1).name("Nom du tag").tagPath("Path du tag").build();
        TagDTO tagDTOMock = TagDTO.builder().name("Nom du tag").tagPath("Path du tag").build();
        tagDTOListMock.add(tagDTOMock);

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(tagEntityMock);
        when(tagRepository.tagArticleExists(tagEntityMock.getId(), articleIdMock)).thenReturn(false);
        when(tagRepository.joinTagToTheArticle(tagEntityMock.getId(), articleIdMock)).thenReturn(1);

        final List<TagDTO> tagDTOList1 = articleController.addTagForArticle(tagDTOListMock, articleIdMock);
        for (TagDTO tagDTO : tagDTOList1) {
            Assertions.assertEquals(tagDTO.getId(), 1);
        }

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(tagEntityMock);
        when(tagRepository.articleExists(articleIdMock)).thenReturn(true);
        when(tagRepository.save(any())).thenReturn(tagEntityMock);
        when(tagRepository.joinTagToTheArticle(tagEntityMock.getId(), articleIdMock)).thenReturn(1);

        final List<TagDTO> tagDTOList2 = articleController.addTagForArticle(tagDTOListMock, articleIdMock);
        for (TagDTO tagDTO : tagDTOList2) {
            Assertions.assertEquals(tagDTO.getId(), 1);
        }
    }

    @Test
    void addTagForArticleException() {

        List<TagDTO> tagDTOListMock = new ArrayList<>();
        Integer articleIdMock  = 5;

        TagEntity tagEntityMock = TagEntity.builder().id(1).name("Nom du tag").tagPath("Path du tag").build();
        TagDTO tagDTOMock = TagDTO.builder().name("Nom du tag").tagPath("Path du tag").build();
        tagDTOListMock.add(tagDTOMock);

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(tagEntityMock);
        when(tagRepository.tagArticleExists(tagEntityMock.getId(), articleIdMock)).thenReturn(true);
        Assertions.assertThrows(ArticleConflictException.class, () -> {
            articleController.addTagForArticle(tagDTOListMock, articleIdMock);
        });

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(tagEntityMock);
        when(tagRepository.tagArticleExists(tagEntityMock.getId(), articleIdMock)).thenReturn(false);
        when(tagRepository.joinTagToTheArticle(tagEntityMock.getId(), articleIdMock))
                .thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(ArticleNotFoundException.class, () -> {
            articleController.addTagForArticle(tagDTOListMock, articleIdMock);
        });

        when(tagRepository.findByTagPath(tagDTOMock.getTagPath())).thenReturn(null);
        when(tagRepository.tagArticleExists(tagEntityMock.getId(), articleIdMock)).thenReturn(true);
        when(tagRepository.articleExists(articleIdMock)).thenReturn(false);
        Assertions.assertThrows(ArticleNotFoundException.class, () -> {
            articleController.addTagForArticle(tagDTOListMock, articleIdMock);
        });
    }

    @Test
    void upArticle() {

        ArticleDTO articleDTOMock = ArticleDTO.builder().id(2).position(3).title("Titre de l'article")
                .projectId(8).proofOfConceptId(null).build();

        when(articleRepository.existsById(articleDTOMock.getId())).thenReturn(true);
        when(articleRepository.save(any())).thenReturn(ArticleMapper.INSTANCE.toEntity(articleDTOMock));
        final ArticleDTO articleDTO = articleController.upArticle(articleDTOMock);
        Assertions.assertEquals(articleDTO.getId(), articleDTOMock.getId());

        when(articleRepository.existsById(articleDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(ArticleNotFoundException.class, () -> {
            articleController.upArticle(articleDTOMock);
        });

    }

    @Test
    void upParagraph() {

        ParagraphDTO paragraphDTOMock = ParagraphDTO.builder().id(9).position(1).subtitle("sout-titre 1")
                .paragraph("Paragraphe 1").articleId(1).build();

        when(paragraphRepository.existsById(paragraphDTOMock.getId())).thenReturn(true);
        when(paragraphRepository.save(any())).thenReturn(ParagraphMapper.INSTANCE.toEntity(paragraphDTOMock));
        final ParagraphDTO paragraphDTO = articleController.upParagraph(paragraphDTOMock);
        Assertions.assertEquals(paragraphDTO.getId(), paragraphDTOMock.getId());

        when(paragraphRepository.existsById(paragraphDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(ParagraphNotFoundException.class, () -> {
            articleController.upParagraph(paragraphDTOMock);
        });
    }


    @Test
    void delArticle() {

        Integer articleIdMock = 1;

        when(articleRepository.deleteArtcileById(articleIdMock)).thenReturn(1);
        final String checkStatusSuccess = articleController.delArticle(articleIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(articleRepository.deleteArtcileById(articleIdMock)).thenReturn(0);
        final String checkStatusFailure = articleController.delArticle(articleIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }

    @Test
    void delPragraph() {

        Integer paragraphIdMock = 1;

        when(paragraphRepository.deleteParagraphById(paragraphIdMock)).thenReturn(1);
        final String checkStatusSuccess = articleController.delPragraph(paragraphIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(paragraphRepository.deleteParagraphById(paragraphIdMock)).thenReturn(0);
        final String checkStatusFailure = articleController.delPragraph(paragraphIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }

    @Test
    void delRessourceByArticle() {

        Integer resourceIdMock = 1;
        Integer articleIdMock = 8;

        when(resourceRepository.deleteResourceForArticle(resourceIdMock, articleIdMock)).thenReturn(1);
        final String checkStatusSuccess = articleController.delRessourceByArticle(resourceIdMock, articleIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(resourceRepository.deleteResourceForArticle(resourceIdMock, articleIdMock)).thenReturn(0);
        final String checkStatusFailure = articleController.delRessourceByArticle(resourceIdMock, articleIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }

    @Test
    void delTagByArticle() {

        Integer tagIdMock = 1;
        Integer articleIdMock = 8;

        when(tagRepository.deleteTagForArticle(tagIdMock, articleIdMock)).thenReturn(1);
        final String checkStatusSuccess = articleController.delTagByArticle(tagIdMock, articleIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(tagRepository.deleteTagForArticle(tagIdMock, articleIdMock)).thenReturn(0);
        final String checkStatusFailure = articleController.delTagByArticle(tagIdMock, articleIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");
    }
}
