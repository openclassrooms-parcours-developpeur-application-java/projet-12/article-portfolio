package com.articleportfolio.controller;

import com.articleportfolio.exception.ResouceNotFoundException;
import com.articleportfolio.exception.ResourceConflictException;
import com.articleportfolio.exception.TagConflictException;
import com.articleportfolio.exception.TagNotFoundException;
import com.articleportfolio.repository.TagRepository;
import com.articleportfolio.service.dto.ResourceDTO;
import com.articleportfolio.service.dto.TagDTO;
import com.articleportfolio.service.mapper.ResourceMapper;
import com.articleportfolio.service.mapper.TagMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class TagControllerTest {

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagController tagController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void upTag() {

        TagDTO tagDTOMock = TagDTO.builder().id(25).name("Nom du tag").tagPath("Path du tag").build();

        when(tagRepository.existsById(tagDTOMock.getId())).thenReturn(true);
        when(tagRepository.save(any())).thenReturn(TagMapper.INSTANCE.toEntity(tagDTOMock));
        final TagDTO tagDTO = tagController.upTag(tagDTOMock);
        Assertions.assertEquals(tagDTO.getId(), tagDTOMock.getId());

        when(tagRepository.existsById(tagDTOMock.getId())).thenReturn(false);
        Assertions.assertThrows(TagNotFoundException.class, () -> {
            tagController.upTag(tagDTOMock);
        });
    }

    @Test
    void delTag() {

        Integer tagIdMock = 1;

        when(tagRepository.tagUseInTheTableArticle(any())).thenReturn(null);
        when(tagRepository.deleteTag(tagIdMock)).thenReturn(1);
        final String checkStatusSuccess = tagController.delTag(tagIdMock);
        Assertions.assertEquals(checkStatusSuccess, "Successful deletion of the image");

        when(tagRepository.tagUseInTheTableArticle(any())).thenReturn(null);
        when(tagRepository.deleteTag(tagIdMock)).thenReturn(0);
        final String checkStatusFailure = tagController.delTag(tagIdMock);
        Assertions.assertEquals(checkStatusFailure, "Failed deletion - image not found");

        when(tagRepository.tagUseInTheTablePointOfInterest(any())).thenReturn("Titre article");
        Assertions.assertThrows(TagConflictException.class, () -> {
            tagController.delTag(tagIdMock);
        });
    }
}
